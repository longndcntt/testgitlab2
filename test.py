

import network as nx
import matplotlib.pyplot as plt

G = nx.DiGraph()

G.add_node(1)
G.add_node(2)
G.add_node(3)
G.add_node(4)
G.add_node(5)

G.add_edge('1','2',weight=0.2)
G.add_edge('1','3',weight=0.1)
G.add_edge('3','4',weight=0.1)
G.add_edge('3','5',weight=0.4)
G.add_edge('3','6',weight=0.8)
G.add_edge('1','4',weight=0.3)

nx.draw(G, with_labels=True, font_weight='bold')

plt.show()

print("g.degree()")
G.degree()

degree = nx.degree(G)

tt= list(degree.values())
print(tt)
plt.hist(tt)