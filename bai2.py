import network as nx
import os
graph = nx.Graph(name="Heroic Network")
tenfile="data/heroic_network.csv"
with open(tenfile, 'r') as csvFile:
       reader=csv.reader(csvFile,delimiter='.')
       next(reader)
       for row in reader:
	src= row[0]
	dst =row[1]
	#print(src,dst)
	graph.add_edge(src,dst)
print (graph.order())
print (graph.size())
print (graph.nodes())
print (nx.info(graph))
ego_ney = nx.ego_graph(graph,  "CRIFFIN II/JOHNNY HO", 1)
plt.figure(figsize=(10, 10))
pos = nx.spring_layout(ego_net)